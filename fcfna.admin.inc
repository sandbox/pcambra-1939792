<?php

/**
 * Admin settings form.
 */
function fcfna_admin_settings() {
  $form = array();
  $role_options = array_map('check_plain', user_roles());
  $form['fcfna_rid'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Role'),
    '#default_value' => variable_get('fcfna_rid', array(DRUPAL_ANONYMOUS_RID => DRUPAL_ANONYMOUS_RID)),
    '#options' => $role_options,
    '#description' => t('Roles that will be affected by the First Click Free functionality.'),
  );

  $type_options = array_map('check_plain', node_type_get_names());
  $form['fcfna_content_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Content types'),
    '#options' => $type_options,
    '#default_value' => variable_get('fcfna_content_types', ''),
    '#description' => t('Content types that will be affected by the First Click Free functionality.'),
  );

  $form['fcfna_limit_views'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Daily limit of node views per user'),
    '#description' => t('Define the number of nodes that a user can see in 24h. Setting this to less than 5 could cause Google to penalize the site. Set 0 to unlimited.'),
    '#default_value' => variable_get('fcfna_limit_views', 0),
  );
  $form = system_settings_form($form);
  $form['#submit'][] = 'fcfna_admin_settings_rebuild_permissions';
  return $form;
}

/**
 * When roles or content type configuration is changed, a rebuild of node access
 * is needed.
 */
function fcfna_admin_settings_rebuild_permissions($form, &$form_state) {
  if ($form['fcfna_rid']['#default_value'] != $form['fcfna_rid']['#value'] || $form['fcfna_content_types']['#default_value'] != $form['fcfna_content_types']['#value']) {
    node_access_needs_rebuild(TRUE);
  }
}
